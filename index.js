const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// Section - MongoDB connection
// Syntax -> mongoose.connect("SRV LINK", {useNewUrlParser: true, useUnifiedTopology: true})

mongoose.connect("mongodb+srv://admin:admin123@zuitt.g9y1zl5.mongodb.net/B217_to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

// Section - Mongoose Schemas

const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Task name is required"]
	},
	status: {
		type: String,
		default: "pending"
	}
});

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "username is required"]
	},
	password: {
		type: String,
		required: [true, "password is required"]
	}
});

// Section - Models
const Task = mongoose.model("Task", taskSchema);

const User = mongoose.model("User", userSchema);

// Section - Creation of to do list routes

app.use(express.json());
app.use(express.urlencoded({extended: true}));


// new task

// post method
app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) =>{
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate found!");
		}
		else{
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, saveTask) =>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created.");
				}
			})
			
		}
	})
})

// get method

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) =>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

// User post method
app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) =>{
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate found!");
		}
		else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, saveUser) =>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New user registered.");
				}
			})
			
		}
	})
})

// User get method

app.get("/signup", (req, res) => {
	User.find({}, (err, result) =>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port}.`))